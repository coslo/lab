


Tutorials
---------

Introduction to molecular simulations
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Before we start, some general advise about the way to organize your work:

- use simple **virtual environments** for Python packages

- keep track of **compilation** steps for C/C++/Fortran codes

- store code / data for **different projects** in **different folders**

- **refactor reusable code** as separate modules / packages

Suggestions:

- **Fortran+Python** is recommended programming language combo - **numba** is ok, too

- *consider* using **jupyter** or **org-mode** notebooks to document your work and workflow

- *consider* using **git** to version-control your simulation codes - probably not your data, though

The molecular simulations ecosystem
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

**Simulation packages**: `LAMMPS <https://www.lammps.org/>`_, `GROMACS <https://www.gromacs.org/>`_, `HOOMD <https://glotzerlab.engin.umich.edu/hoomd-blue/>`_, ...

- massively parallel

- GPU acceleration

- focus on MD, less on MC

**Simulation frameworks**: `ASE <https://wiki.fysik.dtu.dk/ase/>`_, `OpenMM <https://openmm.org/>`_, `PLUMED <https://plumed.org>`_, `atooms <https://framagit.org/atooms/atooms>`_, ...

- high-level management of simulation objects

- develop algorithm on top of existing packages

- abstract out details about simulation backends

Traditional **structure of a simulation code**:

- *Setup*: initial configuration, interaction model, ...

- *Simulation*: equilibration vs. production

- *Analysis*: post-processing vs. on-the-fly calculations

.. image:: code.png

Several stand-alone **setup** and **post-processing** codes, including **visualization,** are freely available too.

The typical workflow from the command line may look like

.. code:: sh

    ./initialize.x < init.inp
    ./simulate.x < sim.inp > sim.out
    ./analyze.x < ana.inp

This is how a typical input script for ``LAMMPS`` looks like (say ``melt.inp``)

.. code:: sh

    # 3d Lennard-Jones melt
    units           lj
    atom_style      atomic

    # Setup
    lattice         fcc 0.8442
    region          box block 0 10 0 10 0 10
    create_box      1 box
    create_atoms    1 box
    mass            1 1.0
    velocity        all create 3.0 87287

    # Interaction parameters
    pair_style      lj/cut 2.5
    pair_coeff      1 1 1.0 1.0 2.5

    # Simulation parameters
    neighbor        0.3 bin
    neigh_modify    every 20 delay 0 check no
    fix             1 all nve

    # Run the simulation and write thermodynamic data on the fly
    thermo          50
    run             250

To setup the system and run the simulation (we assume the executable is ``./lammps``)

.. code:: sh

    ./lammps < melt.inp

Most modern packages offer a native Python interface to run the simulation - that's how a ``hoomd`` script would like, for instance:

.. code:: python

    import hoomd

    gpu = hoomd.device.GPU()
    sim = hoomd.Simulation(device=gpu, seed=1)
    mc = hoomd.hpmc.integrate.ConvexPolyhedron()
    mc.shape['octahedron'] = dict(vertices=[
        (-0.5, 0, 0), (0.5, 0, 0),
        (0, -0.5, 0), (0, 0.5, 0),
        (0, 0, -0.5), (0, 0, 0.5)])
    sim.operations.integrator = mc
    sim.create_state_from_gsd(filename='init.gsd')
    sim.run(100e6)

Getting ready
^^^^^^^^^^^^^

Today, we will deal with a typical use case: **adapt, wrap and run a third party simulation code**. We will use the reusable libraries provided by Allen & Tildesley, written in Fortran and available on their `code repository <https://github.com/Allen-Tildesley>`_, to carry out a first Monte Carlo (MC) simulation of a **fluid of hard spheres**. In a second stage, we will wrap their Fortran code with ``f2py-jit``.

Clone the Allen-Tildesley code repository

.. code:: bash

    git clone https://github.com/Allen-Tildesley/examples.git allen_tildesley

It contains both Fortran and Python codes. A detailed guide to use their codes is available `here <https://github.com/Allen-Tildesley/examples/blob/master/GUIDE.md>`_. For the moment, we do not care much about how the simulation code does what it does - we just want to make it run!

We will also need ``f2py-jit`` to wrap the Fortran code in Python. Make sure you have numpy version higher than 1.25.1, otherwise f2py `will not work correctly <https://github.com/numpy/numpy/pull/24134>`_ with the Allen-Tildesley core. Run this command in your virtual environment.

.. code:: sh

    pip install numpy>=1.25.1
    pip install f2py-jit

Later on, we will use a nice Python package called ``argh`` to create command-line interfaces so let's install it

.. code:: bash

    pip install argh

This is a self-contained ``requirements.txt`` file to set up a minimal working environment

.. code:: conf

    numpy>=1.25.1
    f2py-jit
    argh

A first Monte Carlo simulation
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

We'll first use their code as it is. To compile you can use the ``scons`` tools they suggest or look at the ``SContruct`` file to understand which Fortran files to grab and compile. We'll need one main file and several modules.

Two steps here:

- **setup**: setup the initial configuration

- **simulation**: run the Monte Carlo simulation

Copy the relevant files in a separate folder. These are the files we need to **set up the initial configuration**

.. code:: bash

    mkdir -p hard_spheres
    cp allen_tildesley/{config_io_module,initialize,initialize_module}.f90 hard_spheres/

.. note::

    The above command will only work with the ``bash`` shell. If it does not work, just copy each file individually.

More in detail:

- ``config_io_module.f90``: module to deal with I/O (ex.: reading/writing configurations to file)

- ``initialize_module.f90``: module with subroutines to set up the initial configuration (ex.: fcc lattice, random configuration without overlaps)

- ``initialize.f90``: main program to produce an initial configuration

This is what we need instead to **run the Monte Carlo simulation** for hard spheres

.. code:: bash

    cp allen_tildesley/{maths_module,averages_module,mc_hs_module}.f90 hard_spheres/
    # Main file
    cp allen_tildesley/mc_nvt_hs.f90 hard_spheres/

More in detail:

- ``maths_module.f90``: subroutines for maths, random numbers, order parameters

- ``averages_module.f90``: module to compute averages on the fly and write them to file

- ``mc_hs_module.f90``: subroutines need for MC simulations of hard spheres (ex.: find overlaps between hard spheres)

- ``mc_nvt_hs.f90``: main driver code for MC simulations of hard spheres

We can now compile the code...

.. code:: bash

    cd hard_spheres/
    gfortran -o initialize.x config_io_module.f90 maths_module.f90 initialize_module.f90 initialize.f90

... and initialize the system

.. code:: bash

    echo '&nml /' | ./initialize.x

::

    initialize
    Sets up initial configuration file for various simulations
    Options for molecules are "atom", "linear", "nonlinear", "chain"
    Particle mass m=1 throughout
    Atoms
    nc =                                                 4
    n =                                                256
    No velocities option selected
    Periodic boundary conditions
    Density                                       0.750000
    Box length                                    6.988644
    Close-packed fcc lattice positions
    Writing configuration to filename cnf.inp

Now we have a ``cnf.inp`` that looks like this

.. code:: bash

    head -n 5 cnf.inp

::

    256
         6.98864412
      -3.0575318336  -3.0575318336  -3.0575318336
      -3.0575318336  -2.1839513779  -2.1839513779
      -2.1839513779  -2.1839513779  -3.0575318336


To change the default parameters, add values to the namelist (look for the ``NAMELIST`` line in ``initialize.f90``)

.. code:: bash

    echo '&nml density=0.7, nc=3 /' | ./initialize.x

or store the namelist in a file and redirect it to ``./initialize.x`` on the command line

.. code:: bash

    ./initialize.x < init.inp

Now, let's **compile the Monte Carlo code**

.. code:: bash

    gfortran -O3 -ffast-math -o mc_nvt_hs.x config_io_module.f90 averages_module.f90 \
    	 maths_module.f90 mc_hs_module.f90 mc_nvt_hs.f90

Run a simulation making 10 "blocks" of 1000 MC steps (each MC step is :math:`N` attempted MC moves, where :math:`N` is the number of particles)

.. code:: bash

    echo '&nml nblock=10, nstep=1000 /' | ./mc_nvt_hs.x

::

    mc_nvt_hs
    Monte Carlo, constant-NVT
    Hard sphere potential
    Diameter, sigma = 1
    Energy, kT = 1
    Number of blocks                                    10
    Number of steps per block                         1000
    Maximum displacement                          0.150000
    Pressure scaling parameter                    0.005000
    Number of particles                                256
    Simulation box length                         6.988644
    Density                                       0.750000
    Initial values
    P                                             0.750000


    Run begins
    Date:                                       2024/03/06
    Time:                                         15:02:48
    CPU time:                                     0.011844

    ===============================================
              Block            Move               P
                              ratio
    -----------------------------------------------
                  1        0.319801        4.854492
                  2        0.313844        4.874023
                  3        0.315969        4.975781
                  4        0.317664        4.794140
                  5        0.312570        4.964843
                  6        0.311680        4.889062
                  7        0.313547        4.992383
                  8        0.313234        5.012304
                  9        0.311969        4.945703
                 10        0.314414        5.078711
    -----------------------------------------------
       Run averages        0.314469        4.938145
         Run errors        0.000783        0.025421
    ===============================================

    Run ends
    Date:                                       2024/03/06
    Time:                                         15:02:52
    CPU time:                                     4.371678


    Final values
    P                                             4.851562
    Program ends

The code is pretty efficient: a typical "good" performance for a **sequential molecular simulation code** is of the order of :math:`10^{-6} s/(\text{steps} \times N)` - the actual performance depends on the thermodynamic state and many other details.

.. code:: python

    f'{4.4 / (256 * 10000):.2g} seconds/step/particle'

::

    1.7e-06 seconds/step/particle

Wrapping the Allen-Tildesley code with f2py-jit
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The Allen-Tildesley code is efficient and pretty easy to reuse. As it stands, however, it has a few shortcomings...

Concerning the interface:

- input parameters handling via **namelists** is a bit awkward, especially from the command line

- handling **I/O** is cumbersome in Fortran (although one can use / adapt their Python code)

Concerning the code:

- it is partly **hard-coded** for 3 dimensions [what is we want to simulate hard disks or hyper-spheres?]

- it uses **global variables**: it would be better to encapsulate them in a class

- system variables are **scattered** between modules and main code

- it uses **single precision**: it would be better to switch to double precision

- some variables are **redundant** (ex. the number of particles ``n`` should be inferred from position array)

Our goal is to replace this main code with a **Python driver** (or "entry-point") to make it easier to control the simulation, change its parameters, write the results to file, etc. The results of the Python driver can be

- trajectory or output files ("artifacts")

- a dictionary of output variables

.. code:: python

    def run(input_file='', nsteps=0):
        # run the simulation for nsteps
        # write trajectory file
        # optionally return a dictionary of results
        return {}

This approach enables us to run the simulation workflow both from the command line as before (by wrapping the entry point using ``argh``) and at a *higher level* directly from Python. The setup stage and on-the-fly analysis may then pass around their results directly *within* Python, possibly caching them through a workflow manager (note the gray IO items in the diagram below - more on this later).

.. image:: code_python.png

We start by copying the relevant files in a new folder, where we will develop our wrapper code

.. code:: bash

    cd ../
    mkdir -p hard_spheres_wrap/
    cp allen_tildesley/{maths_module,mc_hs_module,mc_nvt_hs}.f90 hard_spheres_wrap/

We will use ``f2py-jit`` to wrap the ``mc_hs_module`` and call its main subroutines. We would like to compile the code like this...

.. code:: python

    import os
    from f2py_jit import jit

    # Compile the relevant modules for hard spheres
    src = 'hard_spheres_wrap'
    f90 = jit([os.path.join(src, 'maths_module.f90'),
               os.path.join(src, 'mc_hs_module.f90')], flags='-O3 -ffast-math')

...but hold on! Before doing that, we'll have to fix an issue with array indexing in ``maths_module.f90``: there is a ``(0:)`` array in ``polyval``, which does not play nice with ``f2py`` (at least with my setup). These are the diffs to apply:

.. code:: conf

    910c910
    <     REAL, DIMENSION(0:), INTENT(in) :: c ! given coefficients (ascending powers of x)
    ---
    >     REAL, DIMENSION(:), INTENT(in) :: c ! given coefficients (ascending powers of x)
    916,917c916,919
    <     f = c(UBOUND(c,1))
    <     DO i = UBOUND(c,1)-1, 0, -1
    ---
    > !    f = c(UBOUND(c,1))
    > !    DO i = UBOUND(c,1)-1, 0, -1
    >     f = c(SIZE(c,1))
    >     DO i = SIZE(c,1), 1, -1
    934c936
    <     REAL, DIMENSION(0:4), PARAMETER :: g = 1.0 / [1,6,120,5040,362880]
    ---
    >     REAL, DIMENSION(5), PARAMETER :: g = 1.0 / [1,6,120,5040,362880]
    957c959
    <     REAL, DIMENSION(0:4), PARAMETER :: g = 1.0 / [1,6,120,5040,362880]
    ---
    >     REAL, DIMENSION(5), PARAMETER :: g = 1.0 / [1,6,120,5040,362880]

You can edit the lines directly in the code, or apply the patch by first storing the lines above in a file (say ``maths_module.patch``) and then executing the command

.. code:: bash

    patch hard_spheres_wrap/maths_module.f90 maths_module.patch

::

    patching file hard_spheres_wrap/maths_module.f90


We patched the code and we are (almost) ready to go! Check that the code is compiled correctly now

.. code:: python

    import os
    from f2py_jit import jit

    # Compile the relevant modules for hard spheres
    src = 'hard_spheres_wrap'
    f90 = jit([os.path.join(src, 'maths_module.f90'),
               os.path.join(src, 'mc_hs_module.f90')], flags='-O3 -ffast-math')

We'll also need an **initial configuration** in a format suitable for the Allen-Tildesley code. We grab the one we used to start the other simulation

.. code:: bash

    cp hard_spheres/cnf.inp .

To read the initial configuration we have two options:

1) wrap the ``config_io_module`` Fortran module and use its subroutine ``read_cnf_atoms()``

2) use the ``read_cnf_atoms()`` Python function provided in ``allen_tildesley/python_examples/config_io_module.py``

We'll follow route 2.

.. code:: bash

    cp allen_tildesley/python_examples/config_io_module.py .

Finally, we will add a driver subroutine to perform the Monte Carlo steps (currently, this is done in the main program). We can

1) add the driver to ``mc_hs_module.f90``

2) create a separate module forthe driver, say ``mc_hs_driver.f90``

We will follow route 2.

.. code:: fortran

    MODULE mc_driver

      USE maths_module, ONLY : random_translate_vector
      USE mc_module, ONLY: overlap_1, r

      IMPLICIT NONE

      REAL :: box         ! Box length
      REAL :: dr_max      ! Maximum MC displacement

    CONTAINS

      SUBROUTINE run(nsteps)
        INTEGER, INTENT(in) :: nsteps
        INTEGER :: i, step, moves
        REAL :: ri(3), acc_ratio
        ! This may be done once at the beginning
        ! Convert positions to box units and PBC
        r(:,:) = r(:,:) / box
        r(:,:) = r(:,:) - ANINT(r(:,:))
        ! Do nsteps MC steps
        moves = 0
        DO step = 1, nsteps
           DO i = 1, SIZE(r, 2)
              ri(:) = random_translate_vector(dr_max/box, r(:,i))
              ri(:) = ri(:) - ANINT(ri(:))
              IF (.NOT. overlap_1(ri, i, box)) THEN
                 r(:,i) = ri(:)
                 moves  = moves + 1
              END IF
           END DO
        END DO
        acc_ratio = REAL(moves) / (nsteps * SIZE(r, 2))
        ! Convert positions back to natural units
        r(:,:) = r(:,:) * box
      END SUBROUTINE run

    END MODULE mc_driver

Here is the Python wrapper. We make a test ride

.. code:: python

    import os
    from f2py_jit import jit
    from config_io_module import read_cnf_atoms

    # Compile the relevant modules for hard spheres
    src = 'hard_spheres_wrap'
    f90 = jit([os.path.join(src, 'maths_module.f90'),
               os.path.join(src, 'mc_hs_module.f90'),
               os.path.join(src, 'mc_hs_driver.f90')], flags='-O3 -ffast-math')

    # Read the input file, this will allocate the arrays too
    n, box, r = read_cnf_atoms('./cnf.inp')

    # Set the positions in mc_module
    f90.mc_module.n = n
    f90.mc_module.r = r.transpose()
    # Set the remaining parameters in mc_driver
    f90.mc_driver.box = box
    f90.mc_driver.dr_max = 0.1

    # Make nsteps MC steps (each one comprises N attempted moves)
    nsteps = 10
    for step in range(nsteps):
        f90.mc_driver.run(1000)

*It works!* Of course, we are not sure yet that the simulation is correct - that should be checked by measuring some physical quantities and comparing them to some reference values (ex. the ones produced by the original code). We will do it in the lab session. But at least we have a working Python driver to carry out MC simulations for hard spheres.

It is easy now to write a **driver** ("entry point") exposing the key simulation parameters, which can be easily controlled via workflow managers or from the command line via ``argh``, if we prefer to use the command line.

This is a self-contained, minimal driver, we save it as ``hs_mc_run.py``

.. code:: python

    import os
    from f2py_jit import jit
    from config_io_module import read_cnf_atoms

    # Compile the relevant modules for hard spheres
    src = 'hard_spheres_wrap'
    f90 = jit([os.path.join(src, 'maths_module.f90'),
               os.path.join(src, 'mc_hs_module.f90'),
               os.path.join(src, 'mc_hs_driver.f90')], flags='-O3 -ffast-math')

    def run_nvt_hs(input_file='./cnf.inp', nsteps=1, dr_max=0.1):
        n, box, r = read_cnf_atoms('./cnf.inp')
        f90.mc_module.r = r.transpose()
        f90.mc_driver.box = box
        f90.mc_driver.dr_max = dr_max
        nsteps = 10
        for step in range(nsteps):
            f90.mc_driver.run(10000)

    if __name__ == '__main__':
        import argh
        argh.dispatch_command(run_nvt_hs)

.. admonition:: Task 1

    Write the final configuration at the end of the simulation in the same format as the input one, to be able to start a new simulation from a previous one.

.. admonition:: Task 2

    Wrap the ``initialize.f90`` main code, along with its modules, and provide a Python entry point ``initialize()``, similar to ``run_nvt_hs()``.

.. admonition:: Task 2

    Add or wrap the missing bits of the main program ``mc_nvt_hs.f90`` in the driver: initial check for overlaps, accumulate averages and acceptance ratios, ...
